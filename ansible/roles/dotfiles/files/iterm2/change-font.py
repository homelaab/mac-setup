#!/usr/bin/env python3.7

'''
The script changes default font in iTerm2 for best work with Powerline10k Zsh Theme
Check this topic - https://gitlab.com/gnachman/iterm2/-/issues/5208
'''

import iterm2

async def main(connection):
  profiles = await iterm2.PartialProfile.async_query(connection)
  for profile in profiles:
    full = await profile.async_get_full_profile()
    await full.async_set_normal_font("MesloLGS NF")

iterm2.run_until_complete(main)

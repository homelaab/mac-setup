# Mac Setup

## About

This project simplifies the process of getting your Mac up and running. It helps to automatically install applications and configurations.

The script installs [Command Line Tools for Xcode](https://developer.apple.com/download/all/?q=command%20line%20tools) and [Ansible](https://docs.ansible.com/ansible/latest/index.html), then uses this tools to continue setup process.

> **_NOTE:_** The setup process is not fully automated yet, so you still need to do some steps manually (e.g. enter sudo password, Apple ID account, or modify Security rules).

## Run

1. Download archive with source code and extract it into some directory
2. Run Terminal app and cd into directory with extracted source code
3. Type command `bash -c ./run.sh` and hit Enter
4. Type password when promt

## Apps

1. Common:
    - [Google Chrome](https://www.google.com/intl/en/chrome/) - [🍺](https://formulae.brew.sh/cask/google-chrome)
    - [KeePassX](https://www.keepassx.org/) - [🍺](https://formulae.brew.sh/cask/keepassx)
    - [Telegram for macOS](https://macos.telegram.org/) - [🍺](https://formulae.brew.sh/cask/telegram)
    - [Dropbox](https://www.dropbox.com/) - [🍺](https://formulae.brew.sh/cask/dropbox)
    - [Google Drive](https://www.google.com/drive/) - [🍺](https://formulae.brew.sh/cask/google-drive)

2. Development
    - [iTerm2](https://www.iterm2.com/) - [🍺](https://formulae.brew.sh/cask/iterm2)
    - [VS Code](https://code.visualstudio.com/) - [🍺](https://formulae.brew.sh/cask/visual-studio-code)
    - [Docker](https://www.docker.com/) - [🍺](https://formulae.brew.sh/formula/docker)
    - [VirtualBox](https://www.virtualbox.org/) - [🍺](https://formulae.brew.sh/cask/virtualbox)
    - [Vagrant](https://www.vagrantup.com/) - [🍺](https://formulae.brew.sh/cask/vagrant)
    - [Packer](https://packer.io/) - [🍺](https://formulae.brew.sh/formula/packer)
    - [DBeaver Community Edition](https://dbeaver.io/) - [🍺](https://formulae.brew.sh/cask/dbeaver-community)
    - [Etcher](https://balena.io/etcher) - [🍺](https://formulae.brew.sh/cask/balenaetcher)

3. Graphics
    - [Inkscape](https://inkscape.org/) - [🍺](https://formulae.brew.sh/cask/inkscape)
    - [Gimp](https://www.gimp.org/) - [🍺](https://formulae.brew.sh/cask/gimp)
    - [Lightshot Screenshot](https://app.prntscr.com/en/) - [🍏](https://apps.apple.com/us/app/lightshot-screenshot/id526298438)

4. Command Line Tools
    - [ranger](https://ranger.github.io/) - [🍺](https://formulae.brew.sh/formula/ranger)
    - [tmux](https://tmux.github.io/) - [🍺](https://formulae.brew.sh/formula/tmux)
    - [ncdu](https://dev.yorhel.nl/ncdu) - [🍺](https://formulae.brew.sh/formula/ncdu)
    - [mas](https://github.com/mas-cli/mas) - [🍺](https://formulae.brew.sh/formula/mas)
    - [jq](https://stedolan.github.io/jq/) - [🍺](https://formulae.brew.sh/formula/jq)
    - [graphviz](https://graphviz.org/) - [🍺](https://formulae.brew.sh/formula/graphviz)
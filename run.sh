#!/usr/bin/env bash

# Install xcode command line tools
xcode-select -p > /dev/null || xcode-select --install

# Install Ansible if not present
export PATH="$HOME/Library/Python/3.8/bin:$PATH"
if [[ ! $(ansible --version) ]]; then
    # Check if Python3 is installed
    pip3 --version && sudo pip3 install --upgrade pip
    pip3 install ansible
fi

# Run ansible tasks
ansible-playbook -c local -i localhost, ansible/playbook.yml
